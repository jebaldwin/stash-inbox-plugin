<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>com.atlassian.stash</groupId>
        <artifactId>stash-parent</artifactId>
        <!-- if you change this, change the stash.version property as well! -->
        <version>3.2.0</version>
    </parent>

    <groupId>com.atlassian.stash.plugin</groupId>
    <artifactId>stash-inbox-plugin</artifactId>
    <version>1.7.8-SNAPSHOT</version>

    <organization>
        <name>Atlassian</name>
        <url>http://www.atlassian.com/stash</url>
    </organization>

    <name>Stash Inbox</name>
    <description>Provides a list of pull requests you need to action in Stash</description>
    <packaging>atlassian-plugin</packaging>

    <scm>
        <connection>scm:git:ssh://git@bitbucket.org/atlassian/stash-inbox-plugin.git</connection>
        <developerConnection>scm:git:ssh://git@bitbucket.org/atlassian/stash-inbox-plugin.git</developerConnection>
      <tag>HEAD</tag>
  </scm>

    <licenses>
        <license>
            <name>Apache License 2.0</name>
            <url>http://www.apache.org/licenses/LICENSE-2.0</url>
            <distribution>repo</distribution>
        </license>
    </licenses>

    <dependencies>
        <!-- stash -->
        <dependency>
            <groupId>com.atlassian.stash</groupId>
            <artifactId>stash-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.stash</groupId>
            <artifactId>stash-spi</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.stash</groupId>
            <artifactId>stash-rest-common</artifactId>
            <scope>provided</scope>
        </dependency>

        <!-- atlassian libs -->
        <dependency>
            <groupId>com.atlassian.soy</groupId>
            <artifactId>soy-template-renderer-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-webresource</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins.rest</groupId>
            <artifactId>atlassian-rest-common</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins.rest</groupId>
            <artifactId>atlassian-rest-module</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-webfragment</artifactId>
            <scope>provided</scope>
        </dependency>

        <!-- others -->
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>servlet-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>commons-lang</groupId>
            <artifactId>commons-lang</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <scope>provided</scope>
        </dependency>

        <!-- tests -->
        <dependency>
            <groupId>com.atlassian.stash</groupId>
            <artifactId>stash-func-test-common</artifactId>
            <scope>test</scope>
            <exclusions>
                <exclusion>
                    <groupId>asm</groupId>
                    <artifactId>asm</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>com.atlassian.stash</groupId>
            <artifactId>stash-page-objects</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.stash.git</groupId>
            <artifactId>pull-requests</artifactId>
            <version>1.2</version>
            <type>zip</type>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-simple</artifactId>
            <version>${slf4j.libversion}</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>maven-stash-plugin</artifactId>
                <extensions>true</extensions>
                <configuration>
                    <instructions>
                        <Private-Package>
                            com.atlassian.stash.plugin.inbox*
                        </Private-Package>
                    </instructions>
                    <products>
                        <product>
                            <id>stash</id>
                            <version>${stash.version}</version>
                            <dataVersion>${stash.data.version}</dataVersion>
                            <output>${project.build.directory}/stash-${stash.version}.log</output>
                            <systemPropertyVariables>
                                <file.encoding>UTF-8</file.encoding>
                                <atlassian.dev.mode>false</atlassian.dev.mode>
                                <java.awt.headless>true</java.awt.headless>
                                <org.slf4j.simpleLogger.defaultLogLevel>WARN</org.slf4j.simpleLogger.defaultLogLevel>
                                <!-- Our func tests don't know how to wait for the system to be available, so force
                                     it to come up synchronously so that Cargo will block for us -->
                                <johnson.spring.lifecycle.synchronousStartup>true</johnson.spring.lifecycle.synchronousStartup>
                            </systemPropertyVariables>
                        </product>
                    </products>
                    <systemPropertyVariables>
                        <xvfb.enable>true</xvfb.enable>
                    </systemPropertyVariables>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <configuration>
                    <excludes>
                        <exclude>it/**/*.java</exclude>
                    </excludes>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-dependency-plugin</artifactId>
                <executions>
                    <execution>
                        <phase>process-test-resources</phase>
                        <goals>
                            <goal>copy-dependencies</goal>
                        </goals>
                        <configuration>
                            <excludeTransitive>false</excludeTransitive>
                            <includeScope>test</includeScope>
                            <includeTypes>zip</includeTypes>
                            <outputDirectory>${project.build.testOutputDirectory}/git</outputDirectory>
                            <stripVersion>true</stripVersion>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <properties>
        <!-- if you change this, change the stash parent version as well! -->
        <stash.version>3.2.0</stash.version>
        <stash.it.version>${stash.version}</stash.it.version>
        <stash.data.version>${stash.version}</stash.data.version>
        <stash.containerId>tomcat7x</stash.containerId>
        <plugin.testrunner.version>1.1</plugin.testrunner.version>
    </properties>

</project>

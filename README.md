# Stash Inbox Plugin

Adds an item in the header which shows the number of pull requests which are awaiting your approval. Clicking on the link opens a dialog with a list of pull requests to action.

Use the handy keyboard shortcut `g+i` to open your inbox!

![screenshot](http://monosnap.com/image/zCjW81Cfrt2eQHWcJZ0SCHQFJ.png)

# Requirements

This plugin requires Stash 2.5 to run. Upgrade or evaluate [Stash](http://www.atlassian.com/stash) today!

# Want to contribute?
Thanks! Please [fork this project](https://bitbucket.org/atlassian/stash-inbox-plugin/fork) and create a pull request to submit changes back to the original project.

### Developer docs

Read up on developing with the [Atlassian plugin SDK](https://developer.atlassian.com/display/DOCS/Getting+Started) for resources on how to make changes and improve this plugin!

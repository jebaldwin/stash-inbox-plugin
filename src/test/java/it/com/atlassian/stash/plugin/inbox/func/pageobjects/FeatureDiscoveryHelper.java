package it.com.atlassian.stash.plugin.inbox.func.pageobjects;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Poller;
import com.google.inject.Inject;
import org.openqa.selenium.By;

import java.util.List;

/**
 * Helper page object class for dealing with dismissing feature dialogs
 */
public class FeatureDiscoveryHelper {
    private static final String INLINE_DIALOG_CLASS = "aui-inline-dialog";
    private static final String CLOSE_BUTTON_CLASS = "feature-discovery-close";
    private static final String ID_PREFIX = "inline-dialog-feature-discovery-";

    @Inject
    PageElementFinder pageElementFinder;

    public void dismissAll() {
        // Feature Discovery Dialogs are inline dialogs, but with an id starting
        // with "inline-dialog-feature-discovery" - so we need to find all inline-dialogs
        // first
        List<PageElement> dialogs = pageElementFinder.findAll(By.className(INLINE_DIALOG_CLASS));
        for (PageElement dialog : dialogs) {
            if (dialog.getAttribute("id").startsWith(ID_PREFIX)) {
                close(dialog);
            }
        }
    }

    public void dismissFeature(String feature) {
        PageElement dialog = getDialog(feature);
        if (dialog != null && dialog.isVisible()) {
            close(dialog);
        }
    }

    public void close(PageElement dialog) {
        PageElement closeButton = dialog.find(By.className(CLOSE_BUTTON_CLASS));
        closeButton.click();
        Poller.waitUntilFalse("dialog should go away", dialog.timed().isVisible());
    }

    private PageElement getDialog(String feature) {
        return pageElementFinder.find(By.id(ID_PREFIX + feature));
    }
}

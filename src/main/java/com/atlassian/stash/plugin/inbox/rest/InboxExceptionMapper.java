package com.atlassian.stash.plugin.inbox.rest;

import com.atlassian.stash.rest.exception.UnhandledExceptionMapper;
import com.atlassian.stash.rest.exception.UnhandledExceptionMapperHelper;
import com.sun.jersey.spi.resource.Singleton;

import javax.ws.rs.ext.Provider;

@Provider
@Singleton
public class InboxExceptionMapper extends UnhandledExceptionMapper {
    public InboxExceptionMapper(UnhandledExceptionMapperHelper helper) {
        super(helper);
    }
}
